/** библиотеки */
import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import * as serviceWorker from './serviceWorker'


/** компоненты */
import { Demo } from './components/Demo'

ReactDOM.render(<Demo />,document.getElementById('root'));

serviceWorker.unregister();
