/**
 * стандартная тема для ThemeProvider
 */
export enum colors {
  white = '#FFFFFF',
  black = '#333333',
  blue = '#0277bd',
  lightBlue = '#666ad1',
  darkBlue = '#002f6c',
  red = '#801336',
  lightRed = '#f1bbd5',
  darkRed = '#2d132c',
}

export default {
  colors,
  main: {
    buttons: {
      main: {
        border: `1px solid ${colors.blue}`,
        borderRadius: '8px',
        backgroundColor: `${colors.blue}`,
        color: `${colors.white}`,
        padding: '12px 18px',
        transition: 'all linear .2s',
        minWidth: '140px',
        content: {},
        hover: {
          backgroundColor: `${colors.lightBlue}`,
          color: `${colors.white}`,
          borderColor: `${colors.lightBlue}`,
        },
        active: {
          backgroundColor: `${colors.darkBlue}`,
          color: `${colors.white}`,
          borderColor: `${colors.darkBlue}`,
        },
        disabled: {
          backgroundColor: `${colors.blue}`,
          color: `${colors.white}`,
          borderColor: `${colors.blue}`,
          cursor: 'default',
          opacity: 0.5,
        },
      },
    },
    block: {
      background: `${colors.white}`,
    },
    typography: {
      text2: {
        fontFamily: "'Muli', sans-serif",
        fontSize: '14px',
        lineHeight: '16px',
        fontWeight: 'bold',
      },
    },
    checkBox: {
      main: {
        color: `${colors.blue}`,
        backgroundColor: `${colors.blue}`,
      },
    },
  },
  secondary: {
    buttons: {
      main: {
        border: `1px solid ${colors.red}`,
        borderRadius: '8px',
        backgroundColor: `${colors.red}`,
        color: `${colors.white}`,
        padding: '12px 18px',
        transition: 'all linear .2s',
        minWidth: '140px',
        content: {},
        hover: {
          backgroundColor: `${colors.lightRed}`,
          color: `${colors.white}`,
          borderColor: `${colors.lightRed}`,
        },
        active: {
          backgroundColor: `${colors.darkRed}`,
          color: `${colors.white}`,
          borderColor: `${colors.darkRed}`,
        },
        disabled: {
          backgroundColor: `${colors.red}`,
          color: `${colors.white}`,
          borderColor: `${colors.red}`,
          cursor: 'default',
          opacity: 0.5,
        },
      },
    },
    block: {
      background: `${colors.black}`,
    },
    typography: {
      text2: {
        fontFamily: "'Muli', sans-serif",
        fontSize: '14px',
        lineHeight: '16px',
        fontWeight: 'bold',
      },
    },
    checkBox: {
      main: {
        color: `${colors.lightRed}`,
        backgroundColor: `${colors.lightRed}`,
      },
    },
  },
};
