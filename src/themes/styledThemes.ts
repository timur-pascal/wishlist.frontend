import styled, { CreateStyled } from '@emotion/styled';
import theme from './default';

export type Theme = typeof theme;

export default styled as CreateStyled<Theme>;